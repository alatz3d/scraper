<?php
use Goutte\Client;
require_once __DIR__ . '/../vendor/autoload.php';

/**
 * Scraper Class: Holds the scrapping process
 */
class Scraper
{
    public $mainURL;

    /**
     * Scraper constructor.
     * @param string $mainURL
     */
    public function __construct($mainURL)
    {
        $this->mainURL = $mainURL;
    }

    public function scrape(){        
        echo "--- Start Videx Scraper ---".PHP_EOL;
        echo "Connecting to: ".$this->mainURL.PHP_EOL;

        $client = new Client();
        // Go to the mainURL
        $crawler = $client->request('GET', $this->mainURL);

        //Extract the values from the DOM: ‘option title’, ‘description’, ‘price’ and ‘discount’
        // Note: As they are already ordered from low to high, I just reverse each array to get the requested order
        $titles = $crawler->filter('.pricing-table')->last()->filter('h3')->each(function ($node) {
            return $node->text();
        });
        $titlesOrdered = array_reverse($titles);
        
        $descriptions = $crawler->filter('.pricing-table')->last()->filter('.package-name')->each(function ($node) {
            return $node->text();
        });
        $descriptionsOrdered = array_reverse($descriptions);

        $prices = $crawler->filter('.pricing-table')->last()->filter('.price-big')->each(function ($node) {
            return $node->text();
        });
        $pricesOrdered = array_reverse($prices);  

        $discounts = $crawler->filter('.pricing-table')->last()->filter('p')->each(function ($node) {
            return $node->text();
        });
        $discountsOrdered = array_reverse($discounts);

        //Fill the result array:
        $resultArray = array();
        $eachResult = array(
            "option_title" => '',
            "description" => '',
            "price" => '',
            "discount" => ''
        );
        // As all the arrays have the same amount of elements, I use the title as reference and fill the result array:
        foreach ($titlesOrdered as $key => $value) {
            //Create a position in the result array with the requested keys and values
            $resultArray[] = $key;
            $resultArray[$key] = $eachResult;
            //Fill each value
            $resultArray[$key]['option_title'] = $value;
            $resultArray[$key]['description'] = $descriptionsOrdered[$key];
            $resultArray[$key]['price'] = $pricesOrdered[$key];
            $resultArray[$key]['discount'] = $discountsOrdered[$key];
        }
        
        //var_dump($resultArray);

        $resultJSON = json_encode($resultArray);
        echo "JSON Result:".PHP_EOL.PHP_EOL;
        print($resultJSON);
        echo PHP_EOL.PHP_EOL."--- Videx Scraper Finished ---".PHP_EOL;
    }

    public static function fromURL(string $mainURL): self
    {
        return new self($mainURL);
    }
}
