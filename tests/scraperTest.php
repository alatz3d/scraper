<?php
use PHPUnit\Framework\TestCase;

/**
 * Dummy tests, no more time left...
 */

final class ScrapeTest extends TestCase
{
    public function testUrlString(): void
    {
        $this->assertEquals(
            'https://videx.comesconnected.com/',
            'https://videx.comesconnected.com/'
        );
    }

    public function testNumber(): void
    {
        $this->assertEquals(
            1,
            1
        );
    }
    
}

?>