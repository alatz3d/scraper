# Description
This is a console application that scrapes the following website url https://videx.comesconnected.com/ and returns a JSON array of all the product options on the page.

# Requirements
The following libraries are required to install and run the included console scraper script:
- PHP 7.3+
- Composer
- Goutte
- PHPUnit (require-dev)

# Installation
Type the following command in your CLI to install the required libraries using composer:
- composer install

# How to run
Type the following command in your CLI to run the script:
- php scrape.php

## Running Unit Test
Type the following command in your CLI to run the test:
- ./vendor/bin/phpunit tests
